# CEntity
***

## Description
***
A set of methodmaps for source engine entities. The methodmap names are similar to their respective source engine class names.

## Included methodmaps
***
Methodmap Name | Description | Filename | Inherts from
:-----------|:-----------|:-----------|:-----------
CEntity | Base methodmap for all entities. | centity.inc |
CWeapon | Methodmap for most weapons | cweapon.inc | CEntity
CBasePlayer | Methodmap for players in all games. | cbaseplayer.inc | CEntity
CCSPlayer | Methodmap for players in CSS and CSGO | ccsplayer.inc | CBasePlayer

## Usage
***
When you include a methodmap, it will automatically include anything it inherits from. For example, if you include cweapon.inc, centity.inc is automatically included.
An example plugin using all of the methodmaps is included.

## Dependencies
***
CEntity will automatically include SDKTools
CCSPlayer will automatically include cstrike, but will temporarily remove `REQUIRE_EXTENSIONS` if it's defined.

## Known issues
***
Weapons spawned via `CEntity(<name>)` will act as deagles. Use `CWeapon(<name>)` to work around this.
You cannot spawn in silenced weapons with any method. You can give players weapons via `GivePlayerWeapon(player, weapon)`